﻿using AukcioSzerver.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver
{
    public class BackgroundWorker
    {
        private IServiceScopeFactory ServiceScopeFactory { get; set; }
        public BackgroundWorker(IServiceScopeFactory scopeFactory)
        {
            ServiceScopeFactory = scopeFactory;
        }
        public void closeAuction(int ItemId)
        {
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();

                var item = context.Items.Find(ItemId);
                Console.WriteLine($"CLOSING AUCTION {ItemId} named {item.Name}");
                if (item== null)
                {
                    Console.Error.WriteLine($"Tried closing auction on item {ItemId} but it was not found in the database.");
                    return;
                }


                if (item.BidOwnerId != null)
                {
                    item.FinalPrice = item.Price;
                    item.WinningBidId = item.BidOwnerId;

                    context.Notifications.Add(new Notification
                    {
                        Date = DateTime.Now,
                        IsRead = false,
                        UserId = item.WinningBidId ?? -1,
                        Message = $"Gratulálunk! Megnyerted a(z) \"{item.Name}\" aukciót {item.FinalPrice} forintért!"
                    });
                    context.Notifications.Add(new Notification
                    {
                        Date = DateTime.Now,
                        IsRead = false,
                        UserId = item.OwnerId,
                        Message = $"Gratulálunk! Sikeresen zárult a(z) \"{item.Name}\" aukció {item.FinalPrice} forintért!"
                    });
                }
                else
                {
                    context.Notifications.Add(new Notification
                    {
                        Date = DateTime.Now,
                        IsRead = false,
                        UserId = item.OwnerId,
                        Message = $"Sajnos a(z) \"{item.Name}\" aukcióra nem licitáltak az aukció lejártáig."
                    });
                }
                context.SaveChanges();
            }
        }
    }
}
