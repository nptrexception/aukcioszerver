﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AukcioSzerver.Models;
using AukcioSzerver.Models.JsonModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AukcioSzerver.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Cookies")]
    public class BidController : ControllerBase
    {
        DatabaseContext _context = null;

        public BidController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult<SuccessResponse>> placeBid(Bid bid)
        {
            var user = await LoginController.GetUserFromCookie(_context, HttpContext);
            var item = await _context.Items.FindAsync(bid.ItemId);
            if (item.OwnerId == user.Id) return new SuccessResponse { Success = false, Response = "Nem licitálhatsz a saját termékedre" };
            bool success = false;
            if (item == null) return new SuccessResponse { Success = false, Response = "Nem található ilyen aukció" };
            if (item.EndDate < DateTime.Now) return new SuccessResponse { Success = false, Response = "Ez az aukció már lejárt" };
            if ((item.MaxBid==null || item.MaxBid < bid.MaxBid) && bid.MaxBid>=item.Minprice)
            {
                if (user.Id != item.BidOwnerId) item.Price = item.MaxBid ?? item.Minprice;
                item.MaxBid = bid.MaxBid;
                item.BidOwnerId = user.Id;
                success = true;
            }else
            if (item.MaxBid > bid.MaxBid && item.Price < bid.MaxBid)
            {
                item.Price = bid.MaxBid + 1;
                success = true;
            }else
            if (item.MaxBid == bid.MaxBid && item.Price < bid.MaxBid)
            {
                item.Price = bid.MaxBid;
                success = true;
            }

            await _context.SaveChangesAsync();
            return success ? new SuccessResponse { Success = true, Response = "Sikeres licitálás" } : new SuccessResponse { Success = false, Response = "Hiba történt" };

        }
    }
}