﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AukcioSzerver.Models;
using AukcioSzerver.Models.JsonModels;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AukcioSzerver.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Cookies")]
    [ApiController]
    public class ItemsController : Controller
    {
        DatabaseContext _context = null;

        public ItemsController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet("list")]
        public async Task<IEnumerable<ItemQueryResponse>> listItems()
        {
            var user = await LoginController.GetUserFromCookie(_context, HttpContext);
            return await (from i in _context.Items select new ItemQueryResponse
            {
                Id = i.Id,
                OwnerId = i.OwnerId,
                Name = i.Name,
                BidOwnerId = i.BidOwnerId,
                Description = i.Description,
                EndDate = i.EndDate,
                FinalPrice = i.FinalPrice,
                Minprice = i.Minprice,
                Price = i.Price,
                Star = i.Star,
                Category = i.Category,
                StarDescription = i.StarDescription,
                StartDate = i.StartDate,
                ThumbnailPicture = i.ThumbnailPicture,
                WinningBidId = i.WinningBidId,
                MaxBid = (user.Id == i.BidOwnerId || user.Admin) ? i.MaxBid : null
            }).ToListAsync();
        }

        [HttpGet("getitem/{id}")]
        public async Task<ActionResult<ItemQueryResponse>> getItem(int id)
        {
            var user = await LoginController.GetUserFromCookie(_context, HttpContext);
            return await (from i in _context.Items
                          where i.Id == id
                          select new ItemQueryResponse
                          {
                              Id = i.Id,
                              OwnerId = i.OwnerId,
                              Name = i.Name,
                              BidOwnerId = i.BidOwnerId,
                              Description = i.Description,
                              EndDate = i.EndDate,
                              FinalPrice = i.FinalPrice,
                              Minprice = i.Minprice,
                              Price = i.Price,
                              Star = i.Star,
                              Category = i.Category,
                              StarDescription = i.StarDescription,
                              StartDate = i.StartDate,
                              ThumbnailPicture = i.ThumbnailPicture,
                              WinningBidId = i.WinningBidId,
                              MaxBid = (user.Id == i.BidOwnerId || user.Admin) ? i.MaxBid : null
                          }).FirstOrDefaultAsync();
        }

        [HttpGet("getpicture/{id}")]
        public async Task<ActionResult<String>> getPicture(int id)
        {
            return (await _context.Items.FindAsync(id))?.Picture;
        }

        [HttpPut]
        public async Task<IActionResult> putItem (ItemCreateData itemData)
        {
            var item = new Item
            {
                Description = itemData.Description,
                Minprice = itemData.Minprice,
                Name = itemData.Name,
                Picture = itemData.Picture,
                ThumbnailPicture = itemData.ThumbnailPicture,
                Category = itemData.Category,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(Math.Abs(itemData.HowManyDays) % 30),
                OwnerId = LoginController.GetUserFromCookie(_context, HttpContext).Id,
                Price = 0
            };
            await _context.Items.AddAsync(item);
            await _context.SaveChangesAsync();
            bool shortauctions = Convert.ToBoolean(Environment.GetEnvironmentVariable("SHORTAUCTIONS"));
            if (shortauctions) Console.WriteLine($"Short auctions mode on! Closing '{item.Name}' at {item.StartDate.AddMinutes(3).ToLocalTime().TimeOfDay}");
            BackgroundJob.Schedule<BackgroundWorker>(x => x.closeAuction(item.Id),(shortauctions)?item.StartDate.AddMinutes(3):item.EndDate);
            return Ok();
        }

        [HttpPost("rate")]
        public async Task<IActionResult> rateItem (ItemRating rating)
        {
            var user = LoginController.GetUserFromCookie(_context, HttpContext);
            var item = await _context.Items.FindAsync(rating.ItemId);
            if (item == null) return BadRequest();
            if (user.Id == item.BidOwnerId && (DateTime.Now > item.EndDate))
            {
                item.Star = Math.Abs(rating.Stars%5);
                item.StarDescription = rating.StarDescription;
            }
            await _context.SaveChangesAsync();
            return Ok();    
        }
        [HttpPost("filter")]
        public async Task<IEnumerable<ItemQueryResponse>> filterItems (ItemFilter filter)
        {
            var items = from i in _context.Items
                        select i;

            if (filter.Price != null)
            {
                items = from i in items
                        where i.Price <= filter.Price
                        select i;
            }
            if (filter.OwnerId != null)
            {
                items = from i in items
                        where i.OwnerId == filter.OwnerId
                        select i;
            }

            if (filter.Name != "" && filter.Name != null)
            {
                items = from i in items
                        where i.Name.ToLower().Contains(filter.Name.ToLower())
                        select i;
            }
            if (filter.Category != "" && filter.Category!= null)
            {
                items = from i in items
                        where i.Category.ToLower().Contains(filter.Category.ToLower())
                        select i;
            }
            if (filter.Description != "" && filter.Description != null)
            {
                items = from i in items
                        where i.Description.ToLower().Contains(filter.Description.ToLower())
                        select i;
            }

            return await (from i in items
                          select new ItemQueryResponse
                          {
                              Id = i.Id,
                              OwnerId = i.OwnerId,
                              Name = i.Name,
                              BidOwnerId = i.BidOwnerId,
                              Description = i.Description,
                              EndDate = i.EndDate,
                              FinalPrice = i.FinalPrice,
                              Minprice = i.Minprice,
                              Price = i.Price,
                              Star = i.Star,
                              Category = i.Category,
                              StarDescription = i.StarDescription,
                              StartDate = i.StartDate,
                              ThumbnailPicture = i.ThumbnailPicture,
                              WinningBidId = i.WinningBidId,
                          }).ToListAsync();
        }
        
    }
}
