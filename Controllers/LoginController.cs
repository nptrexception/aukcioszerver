﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using AukcioSzerver.Models;
using AukcioSzerver.Models.JsonModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace AukcioSzerver.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Cookies")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly DatabaseContext _context;
        public LoginController(DatabaseContext context)
        {
            _context = context;
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<object>> Login(LoginCredentials loginCredentials)
        {

            

            if (!UsernameExists(_context, loginCredentials.Username))
            {

                Console.WriteLine("$$$Login denied, user not found ");
                return Unauthorized();
            }

            var user = GetUser(_context, loginCredentials.Username);
            var passwordMatches = new PasswordHasher().Check(user.Password, loginCredentials.Password);

            if (!passwordMatches)
            {
                Console.WriteLine("$$$Login denied, password doesn't match");
                return Unauthorized();
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(
              claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var authProperties = new AuthenticationProperties();

            await HttpContext.SignInAsync(
              CookieAuthenticationDefaults.AuthenticationScheme,
              new ClaimsPrincipal(claimsIdentity),
              authProperties);

            return new { 
                Id = user.Id
            };
        }

        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPut]
        public async Task<ActionResult<SuccessResponse>> RegisterUser (User user)
        {
            var requiredDataMissing = user.Username == null || user.Username == ""
                                   || user.Name == null || user.Name == ""
                                   || user.Password == null || user.Password=="" 
                                   || user.Email == null || user.Email =="" 
                                   || user.Phone == null || user.Phone == "" 
                                   || user.Address == null || user.Address=="";
            if (requiredDataMissing)
            {
                return new SuccessResponse { Success = false, Response = "Hiányos adatok" };
            }

            if (UsernameExists(_context, user.Username))
            {
                return new SuccessResponse { Success = false, Response = "Felhasználónév foglalt" };
            }

            user.Admin = false;

            var hashedPass = new PasswordHasher().Hash(user.Password);

            //var checksOut = new PasswordHasher().Check(hashedPass, user.Password);
            //Console.WriteLine("Registration password checks out: " + checksOut.ToString());

            user.Password = hashedPass;

            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return new SuccessResponse { Success = true, Response = "Sikeres regisztráció" };
        }

        [HttpGet("unauthorized")]
        [AllowAnonymous]
        public IActionResult Throw401()
        {
            return Unauthorized();
        }

        [HttpPost("checkpassword")]
        public async Task<ActionResult<SuccessResponse>> checkPassword (LoginCredentials password)
        {
            var user = await GetUserFromCookie(_context, HttpContext);

            var passwordMatches = new PasswordHasher().Check(user.Password, password.Password);

            return passwordMatches ? new SuccessResponse { Success = true, Response = "Helyes jelszó" } : new SuccessResponse { Success = false, Response = "Helytelen jelszó" };
        }


        public static bool UsernameExists (DatabaseContext context, string name)
        {
            return context.Users.Any(e => e.Username == name);
        }

        public static User GetUser (DatabaseContext context, string name)
        {
            return context.Users.Where(e => e.Username == name).First();
        }

        public static async Task<User> GetUserFromCookie(DatabaseContext dbContext, HttpContext httpContext)
        {
            var id = httpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            return await dbContext.Users.FindAsync(Convert.ToInt32(id));
        }
    }





    public sealed class PasswordHasher
    {
        private const int SaltSize = 16; // 128 bit 
        private const int KeySize = 32; // 256 bit

        public string Hash(string password)
        {
            using (var algorithm = new Rfc2898DeriveBytes(
              password,
              SaltSize,
              10000,
              HashAlgorithmName.SHA512))
            {
                var key = Convert.ToBase64String(algorithm.GetBytes(KeySize));
                var salt = Convert.ToBase64String(algorithm.Salt);

                return $"{10000}.{salt}.{key}";
            }
        }

        public bool Check(string hash, string password)
        {
            var parts = hash.Split('.', 3);

            if (parts.Length != 3)
            {
                throw new FormatException("Unexpected hash format. " +
                  "Should be formatted as `{iterations}.{salt}.{hash}`");
            }

            var iterations = Convert.ToInt32(parts[0]);
            var salt = Convert.FromBase64String(parts[1]);
            var key = Convert.FromBase64String(parts[2]);

            using (var algorithm = new Rfc2898DeriveBytes(
              password,
              salt,
              iterations,
              HashAlgorithmName.SHA512))
            {
                var keyToCheck = algorithm.GetBytes(KeySize);

                var verified = keyToCheck.SequenceEqual(key);

                return verified;
            }
        }
    }
}