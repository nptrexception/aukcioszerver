﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AukcioSzerver.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using AukcioSzerver.Models.JsonModels;

namespace AukcioSzerver.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Cookies")]
    public class UsersController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public UsersController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Users
        //[AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserQueryResponse>>> GetUsers()
        {
            var user = await LoginController.GetUserFromCookie(_context, HttpContext);

            if (user.Admin)
            {
                var usersList = from u in _context.Users
                                select new UserQueryResponseAdmin
                                {
                                    Address = u.Address,
                                    Name = u.Name,
                                    Email = u.Email,
                                    Id = u.Id,
                                    Phone = u.Phone,
                                    Username = u.Username,
                                    Admin = u.Admin
                                };
                return await usersList.ToListAsync();
            }
            else
            {
                var usersList = from u in _context.Users
                                select new UserQueryResponse
                                {
                                    Name = u.Name,
                                    Email = u.Email,
                                    Id = u.Id,
                                    Username = u.Username
                                };
                return await usersList.ToListAsync();
            }
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserQueryResponse>> GetUser(long id)
        {
            var user = await LoginController.GetUserFromCookie(_context, HttpContext);

            if (user.Admin || user.Id == id)
            {
                var usersList = from u in _context.Users
                                where u.Id == id
                                select new UserQueryResponseAdmin
                                {
                                    Address = u.Address,
                                    Name = u.Name,
                                    Email = u.Email,
                                    Id = u.Id,
                                    Phone = u.Phone,
                                    Username = u.Username,
                                    Admin = u.Admin
                                };
                return usersList.FirstOrDefault();
            }
            else
            {
                var usersList = from u in _context.Users
                                where u.Id == id
                                select new UserQueryResponse
                                {
                                    Name = u.Name,
                                    Email = u.Email,
                                    Id = u.Id,
                                    Username = u.Username
                                };
                return usersList.FirstOrDefault();
            }
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        /*[HttpPut("{id}")]
        public async Task<IActionResult> PutUser(long id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> ModifyUser(User user)
        {
            var currUser = await LoginController.GetUserFromCookie(_context, HttpContext);
            if (currUser.Id== user.Id || currUser.Admin)
            {
                var dbUser = await _context.Users.FindAsync(user.Id);
                if (dbUser == null) return BadRequest();
                if (user.Address != null && user.Address != "") dbUser.Address = user.Address;
                if (user.Name != null && user.Name != "") dbUser.Name = user.Name;
                if (user.Phone != null && user.Phone != "") dbUser.Phone = user.Phone;
                if (user.Email != null && user.Email != "") dbUser.Email = user.Email;
                if (user.Password != null && user.Password != "") dbUser.Password = new PasswordHasher().Hash(user.Password);
                await _context.SaveChangesAsync();
                return Ok();
            } else
            {
                return Forbid();
            }
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var currUser = await LoginController.GetUserFromCookie(_context, HttpContext);
            if (currUser.Id == id || currUser.Admin)
            {
                _context.Users.Remove(await _context.Users.FindAsync(id));
                await _context.SaveChangesAsync();
                return Ok();
            }
            else
            {
                return Forbid();
            }
        }

        private bool UserExists(long id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
