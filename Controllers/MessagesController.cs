﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AukcioSzerver.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AukcioSzerver.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Cookies")]
    [ApiController]
    public class MessagesController : ControllerBase
    {

        DatabaseContext _context = null;

        public MessagesController(DatabaseContext context)
        {
            _context = context;
        }


        // GET: api/Messages/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Message>> Get(int id)
        {
            return await _context.Messages.FindAsync(id);
        }

        [HttpGet("item/{itemid}")]
        public async Task<IEnumerable<Message>> GetFromItem(int itemid)
        {
            return await (from m in _context.Messages where m.ItemId == itemid select m).ToListAsync();
        }

        // PUT: api/Messages
        [HttpPut("{id}")]
        public async Task<IActionResult> Post(int id, [FromBody]string text)
        {
            if (await _context.Items.FindAsync(id) == null) return BadRequest();
            await _context.Messages.AddAsync(new Message
            {
                ItemId = id,
                Text = text,
                Time = DateTime.Now,
                UserId = (await LoginController.GetUserFromCookie(_context, HttpContext)).Id
            });
            await _context.SaveChangesAsync();
            return Ok();
        }

        // DELETE: api/Messages/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var msg = await _context.Messages.FindAsync(id);
            var user = await LoginController.GetUserFromCookie(_context, HttpContext);
            if (user.Id != msg.UserId && !user.Admin) return Forbid();

            _context.Messages.Remove(msg);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
