﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AukcioSzerver.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AukcioSzerver.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Cookies")]
    public class NotificationsController : ControllerBase
    {
        DatabaseContext _context = null;

        public NotificationsController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet("list")]
        public async Task<IEnumerable<Notification>> listNotifications()
        {
            int id = (await LoginController.GetUserFromCookie(_context, HttpContext)).Id;
            var list = await (from n in _context.Notifications where n.UserId == id && !n.IsRead select n).ToListAsync();
            return list;

        }

        [HttpGet("setread")]
        public async Task<IActionResult> setNotificationsRead()
        {
            int id = (await LoginController.GetUserFromCookie(_context, HttpContext)).Id;

            var list = await (from n in _context.Notifications where n.UserId == id && !n.IsRead select n).ToListAsync();
            list.ForEach(n => n.IsRead = true);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}