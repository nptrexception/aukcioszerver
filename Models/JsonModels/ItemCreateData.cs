﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class ItemCreateData
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public string ThumbnailPicture { get; set; }
        public int HowManyDays { get; set; } //Mennyi ideig tartson?
        public int Minprice { get; set; }
        public string Category { get; set; }
    }
}
