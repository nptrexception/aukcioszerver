﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class UserQueryResponseAdmin : UserQueryResponse
    {
        public string Address { get; set; }
        public string Phone { get; set; }

        public bool Admin { get; set; }
    }
}
