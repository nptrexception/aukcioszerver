﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class Bid
    {
        //public int OwnerId { get; set; }
        public int ItemId { get; set; }
        public int MaxBid { get; set; }
    }
}
