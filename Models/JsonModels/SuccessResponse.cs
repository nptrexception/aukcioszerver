﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class SuccessResponse
    {
        public bool Success { get; set; }
        public string Response { get; set; }
    }
}
