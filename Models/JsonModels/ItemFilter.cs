﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class ItemFilter
    {

        public int? OwnerId { get; set; }
        public string Name { get; set; }
        public int? Price { get; set; } //Jelenlegi ár
        public string Category { get; set; }
        public string Description { get; set; }

    }
}
