﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class ItemRating
    {
        public int ItemId { get; set; }
        public int Stars { get; set; }
        public string StarDescription { get; set; }
    }
}
