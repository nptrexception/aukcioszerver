﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models.JsonModels
{
    public class ItemQueryResponse
    {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbnailPicture { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Price { get; set; } //Jelenlegi ár
        public int Minprice { get; set; } //Minimálár ami alatt nem ad el
        public int? MaxBid { get; set; } //Jelenlegi legnagyobb licit 
        public int? BidOwnerId { get; set; }
        public string Category { get; set; }
        public int? FinalPrice { get; set; }
        public int? WinningBidId { get; set; }
        public int? Star { get; set; }
        public string StarDescription { get; set; }
    }
}
