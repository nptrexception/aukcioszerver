﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models
{
    public class User
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        public string Password { get; set; }
        [MaxLength(60)]
        public string Name { get; set; }
        [MaxLength(60)]
        public string Username { get; set; }
        [MaxLength(200)]
        public string Address { get; set; }
        [MaxLength(20)]
        public string Phone { get; set; }
        public bool Admin { get; set; }
    }
}
