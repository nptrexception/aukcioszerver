﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models
{
    public class Item
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int OwnerId { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public string ThumbnailPicture { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Price { get; set; }
        public int Minprice { get; set; }
        public int? MaxBid { get; set; }
        [ForeignKey("User")]
        public int? BidOwnerId { get; set; }
        public string Category { get; set; }
        public int? FinalPrice { get; set; }
        [ForeignKey("User")]
        public int? WinningBidId { get; set; }
        public int? Star { get; set; }
        [MaxLength(500)]
        public string StarDescription { get; set; }
    }
}
