﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models
{
    public class Notification
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        public bool IsRead { get; set; }

        public DateTime Date { get; set; }

        public string Message { get; set; }
    }
}
