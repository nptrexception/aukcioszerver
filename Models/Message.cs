﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AukcioSzerver.Models
{
    public class Message
    {
        public int Id { get; set; }
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        public DateTime Time { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Text { get; set; }
    }
}
